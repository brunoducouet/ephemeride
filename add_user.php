<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta content="text/html; Charset=UTF-8" http-equiv="Content-Type" />
    <meta name="viewport" content="width=device-width">
    <script async src="./lib/javascript.js"></script>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="icon" type="image/x-icon" href="../favicon.ico">
    <title>Éphéméris</title>
    <?php
        include_once('lib/style.css');
    ?>
</head>
<body>
    <h1>Ephêmeris</h1>
    <?php
    $_SESSION['log']="";
    // Création d'un utilisateur
        if (isset($_POST['mysql_admin'])) {
            $mysql_admin = trim($_POST['mysql_admin']);
            $mysql_pwd   = trim($_POST['mysql_pwd']);
            $eph_name    = trim($_POST['eph_name']);
            $eph_pwd   = trim($_POST['eph_pwd']);
            @$mysqli = new mysqli('localhost', $mysql_admin, $mysql_pwd, "");
            exec('cp ./lib/ephemeride.sql ./lib/eph_user.sql');
            if ($mysqli->connect_errno) { 
                $_SESSION['log'] = $_SESSION['log']."<span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span>
                <span class=logs>".$mysqli->connect_error."</span>";
            }
            else {
                system('cp ./lib/ephemeride.sql ./lib/eph_user.sql', $retval);
                
                if ($retval == 0) { $_SESSION['log'] = $_SESSION['log']."<span class=green><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#good'/></svg></span>
                <span class=logs> Création du script SQL OK</span><br/>"; }
                else { $_SESSION['log'] = $_SESSION['log'].$retval." <span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span>
                <span class=logs> Création du script SQL OK</span><br/>"; } 
                
                system("replace -s 'ephemeride' 'eph_$eph_name' 'user' '$eph_name' 'pwd' '$eph_pwd' -- './lib/eph_user.sql'", $retval);
                if ($retval == 0) { $_SESSION['log'] = $_SESSION['log']."<span class=green><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#good'/></svg></span>
                <span class=logs> Modification du script SQL pour $eph_name</span><br/>"; }
                else { $_SESSION['log'] = $_SESSION['log']."<span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span>
                <span class=logs> Modification du script SQL pour $eph_name</span><br/>"; } 
                
                system("mysql -u $mysql_admin -p$mysql_pwd < ./lib/eph_user.sql", $retval);
                if ($retval == 0) { $_SESSION['log'] = $_SESSION['log']."<span class=green><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#good'/></svg></span>
                <span class=logs> Création de la base pour $eph_name</span><br/>"; }
                else { $_SESSION['log'] = $_SESSION['log']."<span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span>
                <span class=logs> Création de la base pour $eph_name</span><br/>"; } 
                
                system('rm ./lib/eph_user.sql', $retval);
                if ($retval == 0) { $_SESSION['log'] = $_SESSION['log']."<span class=green><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#good'/></svg></span>
                <span class=logs> Suppression du script utilisateur</span><br/>"; }
                else { $_SESSION['log'] = $_SESSION['log']."<span class=red><svg viewBox='0 0 15 15' class='l_icon'><use xlink:href='#bad'/></svg></span>
                <span class=logs> Suppression du script utilisateur</span><br/>"; }
                
                $mysqli->close();
            }
        }
    ?>

    <section id='add-items'>
        <h2>Ajouter un utilisateur</h2>
        <fieldset style="width: 90%; margin: auto;">
            <form method=post class=form>
                <ol>
                <li>
                    <label for=mysql_admin>Admin Mysql</label>
                    <input type=text name=mysql_admin size=15 maxlength=15 id=mysql_admin required="required" placeholder="Nom de l'admin Mysql" />
                </li>
                <li>
                    <label for=mysql_pwd>Mdp Admin Mysql</label>
                    <input type=password name=mysql_pwd size=15 maxlength=15 id=mysql_pwd required="required" placeholder="Mot de passe de l'admin Mysql" />
                </li>
                <li>
                    <label for=eph_name>Nom</label>
                    <input type=text name=eph_name size=15 maxlength=15 id=eph_name required="required" placeholder="Nom du nouvel utilisateur" />
                </li>
                <li>
                    <label for=eph_pwd>Mot de passe</label>
                    <input type=password name=eph_pwd size=15 maxlength=15 id=eph_pwd required="required" placeholder="Mot de passe du nouvel utilisateur" />
                </li>
                </ol>
                <button type=submit id=submit value=Submit>Créer l'utilisateur</button>
            </form>
        </fieldset>
    </section>
    <?php 
        if ($_SESSION['log']!="") {
            echo "<div id=logs>".$_SESSION['log']."</div>";
            $_SESSION['log']="";
        }
    ?>
    
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="10px" height="10px" viewBox="0 0 10 10" enable-background="new 0 0 10 10" xml:space="preserve" class="dont-render-dawg">
    <defs>
        <path id="good" d="M3.5 1A2.506 2.506 0 0 0 1 3.5v9C1 13.876 2.124 15 3.5 15h9c1.376 0 2.5-1.124 2.5-2.5v-9C15 2.124 13.876 1 12.5 1zm0 1h9c.84 0 1.5.66 1.5 1.5v9c0 .84-.66 1.5-1.5 1.5h-9c-.84 0-1.5-.66-1.5-1.5v-9C2 2.66 2.66 2 3.5 2z M14.5 2.5l-6 6-2-2-2 2 4 4 6-6z"/>
        <path id="bad" d="M4 1C2.338 1 1 2.338 1 4v6c0 1.662 1.338 3 3 3h8c1.662 0 3-1.338 3-3V4c0-1.662-1.338-3-3-3zm2 3c.558 0 1.031.473 1.031 1.031V6c0 .558-.473 1-1.031 1-.558 0-1-.442-1-1v-.969C5 4.473 5.442 4 6 4zm4 0c.558 0 1 .473 1 1.031V6c0 .558-.442 1-1 1s-1-.442-1-1v-.969C9 4.473 9.442 4 10 4zM8 8.031c3.256 0 5 .874 5 1.406v.5c-.997-.636-4.016-.906-5-.906s-3.805-.062-5 .906v-.5c0-.68 1.744-1.406 5-1.406zM8 14c-5 0-5 1-5 1 0 1 1 1 1 1h8c1 0 1-1 1-1s0-1-5-1z"/>
    </defs>
</svg>
    <footer>
    </footer>
  </body>
</html>

