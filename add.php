<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
    
<?php include_once('lib/head.php'); ?>    
    <section id='add-items'>
        <h2>Ajout d'événement</h2>
        <fieldset class=res>
            <form class=form method=post name=add_form>
                <ol>
                <li><label for=date>Date</label>
                    <input id=date type=date value="<?php echo date('Y-m-d'); ?>" name=date>
                </li>
                
                <li><label for=cat>Catégorie</label>
                <select required name=cat id=cat onchange="
                    url='inc/ajax_select.php?type='+document.getElementById('cat').value;
                    envoieRequete('lib/ajax_select.php?type='+document.getElementById('cat').value,sub_cat);
                    ">
                    <option value="">Catégorie obligatoire</option>
                    <?php
                        $eph->liste_cat(0);
                    ?>
                </select>
                </li>
                
                <li><label for=sub_cat>Sous catégorie</label>
                <select name=sub_cat id=sub_cat>
                    <option value="">&nbsp;</option>
                </select>
                </li>

                <li><label for=n_desc style="width:75%;">Description</label>
                
                <svg class="img_ta" viewBox="0 0 15 15" onclick='document.getElementById("n_desc").value = document.getElementById("n_desc").value+"<br/>\n";
                document.getElementById("n_desc").focus();
                document.getElementById("n_desc").setSelectionRange(document.getElementById("n_desc").value.length,document.getElementById("n_desc").value.length);
                '><use xlink:href="#enter"/></svg>
                <svg class="img_ta" viewBox="0 0 15 15" onclick='document.getElementById("n_desc").value = document.getElementById("n_desc").value+"*";
                document.getElementById("n_desc").focus();
                document.getElementById("n_desc").setSelectionRange(document.getElementById("n_desc").value.length,document.getElementById("n_desc").value.length);
                '><use xlink:href="#tag"/></svg>
                
                <textarea id=n_desc name=n_desc rows=5 cols=40 maxlength =25000 wrap=hard></textarea>
                </li>
                </ol>
                <svg class="img_tc" onclick="document.forms.add_form.submit();" viewBox="1 1 15 15"><use xlink:href="#add_ev"/></svg>
            </form>
        </fieldset>
    </section>
    
    <?php $eph->log(); ?>
    
    <footer>
    </footer>
  </body>
</html>
