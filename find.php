<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
    
    <?php include_once('lib/head.php'); ?>
    <section id='Recherche'>
        <h2>Rechercher</h2>
        <?php
    // Recherche d'évenement par catégorie
            if (!empty($_POST['find_cat'])) {
                $eph->find_by_cat(intval($_POST['find_cat']),$_POST['debut_r'],$_POST['fin_r']);
            }

    // Recherche d'évenement par tags
            if (!empty($_POST['find_tag'])) {
                $eph->find_by_tag(intval($_POST['find_tag']),$_POST['debut_r'],$_POST['fin_r']);
            }
            
            $res = $eph->minmax();
        ?>
        <form class='form' name='find_form' method='post'>
        <fieldset class=res>
        <legend class=legend>Dates de début et de fin</legend>
            <ol>
            <li>
            <output class=w20 name="debut" id="debut"><?php echo "$res[min_d]" ?></output>
            <input class=w20 type="range" name="debut_r" min='<?php echo "$res[min]" ?>' max='<?php echo "$res[max]" ?>' value='<?php echo "$res[min]" ?>'                                                                    
                    oninput="this.form.debut.value=convertTimestamp(this.value)">
            <input class=w20 type="range" name="fin_r" min='<?php echo "$res[min]" ?>' max='<?php echo "$res[max]" ?>' value='<?php echo "$res[max]" ?>'                                                                    
                oninput="this.form.fin.value=convertTimestamp(this.value)">
            <output class=w20 name="fin" id="fin"><?php echo "$res[max_d]" ?></output>
            </li>
            </ol>
        </fieldset>

        <fieldset class=res>
            <legend class=legend>Recherche par catégorie/sous catégorie</legend>
                <ol>
                    <li><label for=find_cat>Catégorie</label>
                        <select name=find_cat id=find_cat onchange="document.forms.find_form.submit();">
                            <?php $eph->list_all_cat() ?>
                            <option selected>&nbsp;</option>
                        </select>
                    </li>
                </ol>
        </fieldset>
        <fieldset class=res>
            <legend class=legend>Recherche par étiquette</legend>
                <ol>
                    <li><label for=find_tag>Étiquette</label>
                        <select name=find_tag id=find_tag onchange="document.forms.find_form.submit();">
                            <?php $eph->list_all_tag() ?>
                            <option selected>&nbsp;</option>
                        </select>
                    </li>
                    
                </ol>
            </form>
        </fieldset>
    </section>
    
    <?php $eph->log(); ?>    
    
    <footer>
    </footer>
  </body>
</html> 
