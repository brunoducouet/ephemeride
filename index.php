<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
    
<?php include_once('lib/head.php'); ?>    
    <section>
        <h2>Prochains événements</h2>
        <fieldset class=res>
            <legend class=legend>Anniversaires</legend>
            <?php
                $eph->liste_birthday();
            ?>
        </fieldset>
        <fieldset class=res>
            <legend class=legend>Autres</legend>
            <?php
                $eph->liste_next_ev();
            ?>
        </fieldset>
    </section>
    
    <?php $eph->log(); ?>
    
    <footer>
    </footer>
  </body>
</html>
